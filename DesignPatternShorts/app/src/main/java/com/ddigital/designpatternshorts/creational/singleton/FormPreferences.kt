package com.ddigital.designpatternshorts.creational.singleton

import android.content.Context
import com.ddigital.designpatternshorts.DPApp
import com.ddigital.designpatternshorts.creational.builder.FormParams

object FormPreferences {

    private val prefs = DPApp.INSTANCE.getSharedPreferences("FORM_PREFS", Context.MODE_PRIVATE)

    fun savePrefs(params: FormParams) {

    }

    fun loadPrefs(): FormParams {
        return FormParams()
    }

}

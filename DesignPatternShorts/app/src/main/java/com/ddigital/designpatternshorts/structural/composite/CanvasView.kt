package com.ddigital.designpatternshorts.structural.composite

import android.content.Context
import android.graphics.Canvas
import android.util.Log
import android.view.View
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class CanvasView(context: Context) : View(context) {

    private var startDrawing = false
    private var compositeDots: CompositeDots? = null

    private var touchX: Float = 0f
    private var touchY: Float = 0f

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)
        if (startDrawing) {
            drawCompositeGraphic(canvas)
            startDrawing = false
        }
    }

    private fun drawCompositeGraphic(canvas: Canvas?) {
        compositeDots?.drawGraphic(canvas)
        Log.i("CanvasView", "drawGraphic: $compositeDots")
    }

    suspend fun drawAt(x: Float, y: Float) {
        withContext(Dispatchers.Default) {
            touchX = x
            touchY = y
            compositeDots = generateCircles(4, 20f, touchX, touchY)
            startDrawing = true
            invalidate()
        }
    }

    private fun generateCircles(max: Int, radius: Float, x: Float, y: Float): CompositeDots {
        val randomNumberOfGraphics = (0..max).random()
        val parentCircle = CompositeDots()
        if (randomNumberOfGraphics == 0) return parentCircle

        repeat(randomNumberOfGraphics) { index ->
            val numCircles = parentCircle.graphicChildren.filterIsInstance<CompositeDots>().size + 1
            val dotSpacing = 60 * index

            if ((0..1).random() > 0) {
                parentCircle.graphicChildren.add(Dot(x + dotSpacing, y, radius = radius))
            } else {
                parentCircle.graphicChildren.add(Dot(x + dotSpacing, y, "#FF6200EE", radius))
                parentCircle.graphicChildren.add(
                    generateCircles(
                        max - 1,
                        radius - 3f,
                        x + dotSpacing,
                        y + numCircles * 50
                    )
                )
            }
        }
        return parentCircle
    }
}
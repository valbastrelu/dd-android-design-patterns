package com.ddigital.designpatternshorts.core

import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.ddigital.designpatternshorts.R

abstract class DiagramMenuActivity : AppCompatActivity() {

    abstract val umlImageName: String
    abstract val containerId: Int

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_diagram, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.diagram -> {
                DiagramDialogFragment(umlImageName).show(supportFragmentManager, "DIAGRAM_FRAGMENT")
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    protected fun addFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .add(containerId, fragment)
            .commitNow()
    }

    protected fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(containerId, fragment)
            .commitNow()
    }
}
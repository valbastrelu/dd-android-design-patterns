package com.ddigital.designpatternshorts.structural.bridge

import android.os.Bundle
import androidx.lifecycle.coroutineScope
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.databinding.ActivityAdapterBinding
import com.ddigital.designpatternshorts.utils.DesignPatternType
import kotlinx.coroutines.flow.collect

class BridgeActivity: DiagramMenuActivity() {

    private val forecastRepository = ForecastRepository()

    //ViewModel

    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.Adapter)
    override val containerId: Int
        get() = -1

    private lateinit var binding: ActivityAdapterBinding
    private val adapter: ForecastRecyclerViewAdapter by lazy {
        ForecastRecyclerViewAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAdapterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerViewWeather.adapter = adapter
        lifecycle.coroutineScope.launchWhenCreated {
            forecastRepository.fetchForecast()
            forecastRepository.weekFlow.collect { forecast ->
                adapter.submitList(forecast)
            }
        }
    }
}


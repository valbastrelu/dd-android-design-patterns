package com.ddigital.designpatternshorts.structural.composite

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint

open class Dot(
    private val touchX: Float,
    private val touchY: Float,
    private val color: String = "#FFBB86FC",
    open var radius: Float = 10f
) : Graphic {

    private val paint: Paint by lazy { Paint() }

    override fun drawGraphic(canvas: Canvas?) {
        val x = touchX
        val y = touchY
        paint.color = Color.parseColor(color)
        canvas?.drawCircle(x, y, radius, paint)
    }

    override fun toString(): String {
        return "Dot(x=$touchX, y=$touchY)"
    }
}
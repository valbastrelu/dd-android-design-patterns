package com.ddigital.designpatternshorts.structural.composite.animation

import android.graphics.Canvas
import android.view.animation.Animation
import android.view.animation.Transformation
import com.ddigital.designpatternshorts.structural.composite.Dot

class DotAnimation(
    private val dot: Dot,
    private val canvas: Canvas?
): Animation() {

    private var oldRad = 0f
    private var newRad = dot.radius

    override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
        val rad = oldRad + ((newRad - oldRad) * interpolatedTime)
        dot.radius = rad
        dot.drawGraphic(canvas)
    }
}
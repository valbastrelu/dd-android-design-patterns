package com.ddigital.designpatternshorts.creational.factoryMethod

import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.ddigital.designpatternshorts.R

class PersonalDetailsFragment(background: Int, message: String) : RegisterFragment(background, message) {

    //TODO Some custom behaviour like having EditText fields

    override fun setBackground(colorId: Int) {
        val background = view?.findViewById<ConstraintLayout>(R.id.factory_first)
        background?.setBackgroundColor(ContextCompat.getColor(requireContext(), colorId))
    }

    override fun setMessage(message: String) {
        val messageView = view?.findViewById<TextView>(R.id.message)
        messageView?.text = getString(R.string.fragment_message, message)
    }
}

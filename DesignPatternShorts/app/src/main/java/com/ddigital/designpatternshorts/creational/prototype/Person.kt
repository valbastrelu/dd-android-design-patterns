package com.ddigital.designpatternshorts.creational.prototype

class Person(
    private val name: String
) : Prototype {

    var owned: Ownership? = null
    var rented : Rental = Rental("Mansion", "Ferrari")

    constructor(newPerson: Person) : this(
        name = newPerson.name
    ) {
        this.owned = newPerson.owned?.clone() as Ownership //new object reference
        this.rented = newPerson.rented                     //reference passed from parameter object
    }

    override fun clone(): Prototype {
        return Person(this)
    }
}
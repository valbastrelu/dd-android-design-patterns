package com.ddigital.designpatternshorts.structural.bridge

import com.ddigital.designpatternshorts.structural.common.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class ForecastRepository {

    private val _weekFlow: MutableStateFlow<List<AbstractDay>> = MutableStateFlow(emptyList())
    val weekFlow: StateFlow<List<AbstractDay>>
        get() = _weekFlow

    suspend fun fetchForecast() {

        val weekForecast = listOf(
            Monday(DayForecast()),
            Tuesday(DayForecast()),
            Thursday(DayForecast()),
            Wednesday(DayForecast()),
            Friday(DayForecast()),
            Saturday(DayForecast()),
            Sunday(DayForecast())
        )
        _weekFlow.emit(weekForecast)
    }
}
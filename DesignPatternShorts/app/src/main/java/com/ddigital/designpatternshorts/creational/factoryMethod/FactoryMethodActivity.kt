package com.ddigital.designpatternshorts.creational.factoryMethod

import android.os.Bundle
import android.widget.Button
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.utils.DesignPatternType

class FactoryMethodActivity : DiagramMenuActivity() {

    private var currentPage: Int = 0

    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.FactoryMethod)
    override val containerId: Int
        get() = R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_factory_method)
        replaceFragment(RegisterFragmentFactory.create(0))

        val buttonNext = findViewById<Button>(R.id.buttonNext)
        buttonNext.setOnClickListener {
            currentPage++
            val fragment = RegisterFragmentFactory.create(currentPage)
            addFragment(fragment)
        }
    }
}
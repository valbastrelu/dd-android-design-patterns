package com.ddigital.designpatternshorts

import android.app.Application

class DPApp: Application() {

    init {
        INSTANCE = this
    }

    companion object {
        lateinit var INSTANCE: DPApp
            private set
    }
}

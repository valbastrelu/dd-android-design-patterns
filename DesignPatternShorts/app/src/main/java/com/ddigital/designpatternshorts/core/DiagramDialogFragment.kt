package com.ddigital.designpatternshorts.core

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.ddigital.designpatternshorts.R
import java.io.IOException
import java.io.InputStream


class DiagramDialogFragment(
    private val image: String
): DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_fragment_diagram, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val imageViewDiagram = view.findViewById<ImageView>(R.id.imageViewUML)
        imageViewDiagram.loadImage(image)
    }

    override fun onResume() {
        super.onResume()
        val params: ViewGroup.LayoutParams = dialog!!.window!!.attributes
        params.width = ViewGroup.LayoutParams.MATCH_PARENT
        params.height = ViewGroup.LayoutParams.MATCH_PARENT
        dialog!!.window!!.attributes = params as WindowManager.LayoutParams
    }

    private fun ImageView.loadImage(image: String) {
        try {
            val ims: InputStream = resources.assets.open("$image.png")
            val drawable = Drawable.createFromStream(ims, null)
            this.setImageDrawable(drawable)
        } catch (ex: IOException) {
            return
        }
    }
}
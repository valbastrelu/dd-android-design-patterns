package com.ddigital.designpatternshorts.host

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.viewpager2.widget.ViewPager2
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.utils.DesignPatternsConstants
import com.ddigital.designpatternshorts.utils.VPLinearAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class HostActivity : AppCompatActivity() {

    private val viewPagerAdapter: VPLinearAdapter by lazy { VPLinearAdapter() }
    private val fragmentPagerAdapter: HostFragmentPagerAdapter by lazy {
        HostFragmentPagerAdapter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_host)

        val tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        val viewPager = findViewById<ViewPager2>(R.id.viewPager2)
        viewPagerAdapter.dpCategories = DesignPatternsConstants.tabs
//        viewPager.adapter = viewPagerAdapter
        viewPager.adapter = fragmentPagerAdapter

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = DesignPatternsConstants.tabTitles[position]
        }.attach()

        tabLayout.setTabIndicatorColorAndText(0)
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tabLayout.setTabIndicatorColorAndText(tab?.position ?: -1)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) = Unit
            override fun onTabReselected(tab: TabLayout.Tab?) = Unit
        })
    }

    private fun TabLayout.setTabIndicatorColorAndText(position: Int) {
        val colorRes = when (position) {
            0 -> R.color.creational_tint
            1 -> R.color.structural_tint
            2 -> R.color.behavioural_tint
            else -> R.color.grey_500
        }
        val normalTabColor =  ContextCompat.getColor(this.context, R.color.grey_500)
        val selectedTabColor =  ContextCompat.getColor(this.context, colorRes)
        this.setSelectedTabIndicatorColor(selectedTabColor)
        this.setTabTextColors(normalTabColor, selectedTabColor)
    }
}

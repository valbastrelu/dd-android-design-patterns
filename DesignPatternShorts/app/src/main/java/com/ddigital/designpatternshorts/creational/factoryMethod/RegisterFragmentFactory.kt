package com.ddigital.designpatternshorts.creational.factoryMethod

import com.ddigital.designpatternshorts.R

class RegisterFragmentFactory {

    companion object {
        fun create(step: Int): RegisterFragment {
            return when (step) {
                0 -> PersonalDetailsFragment(R.color.purple_200, "Personal Details")
                else -> PreferencesFragment(R.color.purple_500, "Preferences")
            }
        }
    }
}
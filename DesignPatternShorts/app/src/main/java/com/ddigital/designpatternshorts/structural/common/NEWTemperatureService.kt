package com.ddigital.designpatternshorts.structural.common

class NEWTemperatureService {

    fun getTemp(day: Day): String {
       return when (day) {
            Day.MONDAY -> "20, 10"
            Day.TUESDAY -> "20, 10"
            Day.THURSDAY -> "20, 4"
            Day.WEDNESDAY -> "31, 9"
            Day.FRIDAY -> "12, 2"
            Day.SATURDAY -> "20, 12"
            Day.SUNDAY -> "2, -10"
        }
    }
}
package com.ddigital.designpatternshorts.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ddigital.designpatternshorts.R

class VPLinearAdapter: RecyclerView.Adapter<VPLinearAdapter.GridPageViewHolder>() {

    var dpCategories = listOf<List<DesignPatternType>>()
    set(value) {
        if (value.isNotEmpty()) {
            field = value
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridPageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.fragment_host_grid_patterns, parent, false)
        return GridPageViewHolder(view)
    }

    override fun onBindViewHolder(holder: GridPageViewHolder, position: Int) {
        holder.bind(dpCategories[position])
    }

    override fun getItemCount(): Int = dpCategories.size

    class GridPageViewHolder(view: View): RecyclerView.ViewHolder(view) {

        private val rvPatterns = itemView.findViewById<RecyclerView>(R.id.rvDesignPatterns)

        fun bind(category: List<DesignPatternType>) {
            TODO("Not yet implemented")
        }

    }
}

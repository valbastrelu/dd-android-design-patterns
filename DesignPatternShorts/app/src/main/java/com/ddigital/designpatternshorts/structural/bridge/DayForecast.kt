package com.ddigital.designpatternshorts.structural.bridge

import com.ddigital.designpatternshorts.structural.common.Day
import com.ddigital.designpatternshorts.structural.common.UvIndex

interface Forecast {
    fun getTempValues(day: Day): Pair<Int, Int>
    fun getUvIndex(day: Day): String
}

//Service api
class DayForecast: Forecast {
    
    override fun getTempValues(day: Day): Pair<Int, Int> {
        return when (day) {
            Day.MONDAY -> Pair(1,5)
            Day.TUESDAY -> Pair(12,15)
            Day.THURSDAY -> Pair(10,20)
            Day.WEDNESDAY -> Pair(21,30)
            Day.FRIDAY -> Pair(20,35)
            Day.SATURDAY -> Pair(21,33)
            Day.SUNDAY -> Pair(18,29)
        }
    }

    override fun getUvIndex(day: Day): String {
        return when (getTempValues(day).second) {
            in 0 .. 5 -> UvIndex.LOW.name
            in 6 .. 20 -> UvIndex.MEDIUM.name
            in 21 .. 36 -> UvIndex.EXTREME.name
            else -> UvIndex.EXTREME.name
        }
    }
}
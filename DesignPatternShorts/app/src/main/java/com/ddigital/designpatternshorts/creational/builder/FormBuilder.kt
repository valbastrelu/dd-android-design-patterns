package com.ddigital.designpatternshorts.creational.builder

class FormBuilder: Builder {

    private var formParams = FormParams()

    override fun addNameField(name: String): FormBuilder {
        formParams.name = name
        return this
    }

    override fun addSurnameField(): FormBuilder {
        formParams.surname = "surname"
        return this
    }

    override fun addAgeField(): FormBuilder {
        formParams.age = "age"
        return this
    }

    override fun addSexField(): FormBuilder {
        formParams.sex = ("sex")
        return this
    }

    override fun addCountryField(): FormBuilder {
        formParams.country = ("country")
        return this
    }

    override fun addAddressField(): FormBuilder {
        formParams.address = ("address")
        return this
    }

    override fun addPhoneField(): FormBuilder {
        formParams.phone = ("phone")
        return this
    }
    
    fun build(): FormFragment {
        return FormFragment().apply {
            this.params = formParams
        }
    }
}

package com.ddigital.designpatternshorts.creational.prototype

interface Prototype {
    fun clone(): Prototype
}
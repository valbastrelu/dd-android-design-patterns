package com.ddigital.designpatternshorts.structural.common

import androidx.annotation.FloatRange
import com.ddigital.designpatternshorts.structural.adapter.TemperatureAdapter

object WeatherService {
    fun getForeCast(): List<DayForecast> {
        return Day.values().map { day ->

//            Old service
//            val temp = TemperatureService.getTemp(day)

//            New Service
            val adapter = TemperatureAdapter(NEWTemperatureService())
            val temp = adapter.getTemp(day)

            DayForecast(day, temp, .5, "", UvIndex.getUvIndexRisk(day.ordinal % 3))
        }
    }
}

data class DayForecast(
    val day: Day = Day.MONDAY,
    val temp: Pair<Int, Int>, // represents High and Low temps
    @FloatRange(from = 0.0, to = 1.0)
    val humidity: Double, // use decimals to represent percentage
    val pressure: String,
    val uvIndex: UvIndex = UvIndex.LOW,
)

data class Temp (
    val min: Int,
    val max: Int
)

enum class Day {
    MONDAY,
    TUESDAY,
    THURSDAY,
    WEDNESDAY,
    FRIDAY,
    SATURDAY,
    SUNDAY
}

enum class UvIndex(
    val value: Int
) {
    LOW(0),
    MEDIUM(1),
    HIGH(2),
    EXTREME(3);

    companion object {
        fun getUvIndexRisk(value: Int): UvIndex {
            return values().find { it.value == value } ?: LOW
        }
    }
}


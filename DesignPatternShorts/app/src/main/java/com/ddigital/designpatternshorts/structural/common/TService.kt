package com.ddigital.designpatternshorts.structural.common

interface TService {
    fun getTemp(day: Day): Pair<Int, Int>
}
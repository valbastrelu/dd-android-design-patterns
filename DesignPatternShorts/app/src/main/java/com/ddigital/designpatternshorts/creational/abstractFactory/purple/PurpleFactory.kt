package com.ddigital.designpatternshorts.creational.abstractFactory.purple

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.ddigital.designpatternshorts.creational.abstractFactory.AbstractColorFactory

class PurpleFactory : AbstractColorFactory {

    override fun createFragment(): Fragment {
        return PurpleFragment()
    }

    override fun createDialogFragment(): DialogFragment {
        return PurpleDialogFragment()
    }
}
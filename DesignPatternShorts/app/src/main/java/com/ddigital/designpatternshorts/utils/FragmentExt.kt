package com.ddigital.designpatternshorts.utils

import android.view.View
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment

fun <T : View> Fragment.find(@IdRes id: Int): T? {
    return this.view?.findViewById(id)
}

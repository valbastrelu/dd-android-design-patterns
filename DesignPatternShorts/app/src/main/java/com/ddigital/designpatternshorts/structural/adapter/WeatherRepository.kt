package com.ddigital.designpatternshorts.structural.adapter

import com.ddigital.designpatternshorts.structural.common.DayForecast
import com.ddigital.designpatternshorts.structural.common.WeatherService
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

class WeatherRepository {

    private val _weekFlow : MutableStateFlow<List<DayForecast>> = MutableStateFlow(emptyList())
    val weekFlow: StateFlow<List<DayForecast>>
        get() = _weekFlow

    suspend fun fetchForecast() {
        _weekFlow.emit(WeatherService.getForeCast())
    }

}
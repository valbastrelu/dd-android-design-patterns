package com.ddigital.designpatternshorts.host

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.utils.DesignPatternType

class HostGridAdapter(
    private val listener: View.OnClickListener
): RecyclerView.Adapter<HostGridAdapter.DPItem>() {

    var dpItem: List<DesignPatternType> = listOf()
        set(value) {
            if (value.isNotEmpty()) {
                field = value
                notifyDataSetChanged()
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DPItem {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_design_pattern, parent, false)
        return DPItem(view)
    }

    override fun onBindViewHolder(holder: DPItem, position: Int) {
        holder.bind(dpItem[position], listener)
    }

    override fun getItemCount(): Int = dpItem.size

    class DPItem(view: View): RecyclerView.ViewHolder(view){

        private val dpButton = itemView.findViewById<Button>(R.id.designPatternButton)

        @SuppressLint("UseCompatLoadingForColorStateLists")
        fun bind(type: DesignPatternType, listener: View.OnClickListener) {
            val designCategoryColor = getDesignCategoryColorType(type)

            dpButton.text = type.prettyName
            dpButton.tag = type
            dpButton.backgroundTintList = itemView.resources.getColorStateList(designCategoryColor)
            dpButton.setOnClickListener(listener)

        }

        private fun getDesignCategoryColorType(type: DesignPatternType): Int {
            return when (type) {
                in DesignPatternType.getCreationals() -> R.color.creational_tint
                in DesignPatternType.getStructurals() -> R.color.structural_tint
                in DesignPatternType.getBehaviourals() -> R.color.behavioural_tint
                else -> R.color.teal_700
            }
        }

    }
}

package com.ddigital.designpatternshorts.creational.abstractFactory

import android.os.Bundle
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.creational.abstractFactory.purple.PurpleFactory
import com.ddigital.designpatternshorts.creational.abstractFactory.teal.TealFactory
import com.ddigital.designpatternshorts.utils.DesignPatternType

class AbstractFactoryActivity : DiagramMenuActivity() {

    private lateinit var factory: AbstractColorFactory

    override val containerId: Int
        get() = R.id.container
    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.AbstractFactory)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abstract_factory)

        val multipleOfTwo = (System.currentTimeMillis() % 2) == 0L
        factory = if (multipleOfTwo) PurpleFactory() else TealFactory()

        if (savedInstanceState == null) {
            replaceFragment(factory.createFragment())
        }
    }
}

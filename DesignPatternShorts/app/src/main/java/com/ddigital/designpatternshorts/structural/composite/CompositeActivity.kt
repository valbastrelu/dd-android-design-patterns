package com.ddigital.designpatternshorts.structural.composite

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MotionEvent
import android.widget.FrameLayout
import androidx.lifecycle.lifecycleScope
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.utils.DesignPatternType


class CompositeActivity : DiagramMenuActivity() {
    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.Composite)
    override val containerId: Int
        get() = R.layout.activity_composite

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_composite)

        val canvasView = CanvasView(this)
        val frame = findViewById<FrameLayout>(R.id.frameLayout)
        frame.addView(canvasView)

        canvasView.setOnTouchListener { view, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_DOWN) {
                val touchX = motionEvent.x
                val touchY = motionEvent.y
                lifecycleScope.launchWhenStarted {
                    canvasView.drawAt(touchX, touchY)
                }
            }
            false
        }
    }
}

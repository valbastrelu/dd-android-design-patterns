package com.ddigital.designpatternshorts.creational.builder

interface Builder {
    fun addNameField(name: String): Builder
    fun addSurnameField(): Builder
    fun addAgeField(): Builder
    fun addSexField(): Builder
    fun addCountryField(): Builder
    fun addAddressField(): Builder
    fun addPhoneField(): Builder
}

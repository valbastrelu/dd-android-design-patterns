package com.ddigital.designpatternshorts.structural.bridge

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ddigital.designpatternshorts.databinding.ItemWeatherDayAdapterBinding
import com.ddigital.designpatternshorts.structural.common.AbstractDay

class ForecastRecyclerViewAdapter :
    ListAdapter<AbstractDay, ForecastRecyclerViewAdapter.DayForecastViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayForecastViewHolder {

        val binding = ItemWeatherDayAdapterBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DayForecastViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DayForecastViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class DayForecastViewHolder(private val binding: ItemWeatherDayAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(day: AbstractDay) {
            binding.apply {
                val temp: Pair<Int, Int> = day.getTemp()
                textTemperatureValue.text = "Max: ${temp.first} / Min: ${temp.second}"
                textDay.text = day::class.simpleName
                textUVindexValue.text = day.getUvIndex()
                textPressureValue.text = "-"
                textHumidityValue.text = "-"
            }
        }
    }

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<AbstractDay>() {
            override fun areItemsTheSame(oldItem: AbstractDay, newItem: AbstractDay): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: AbstractDay, newItem: AbstractDay): Boolean =
                oldItem == newItem

        }
    }
}
package com.ddigital.designpatternshorts.structural.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ddigital.designpatternshorts.databinding.ItemWeatherDayAdapterBinding
import com.ddigital.designpatternshorts.structural.common.DayForecast

class WeatherRecyclerViewAdapter :
    ListAdapter<DayForecast, WeatherRecyclerViewAdapter.DayForecastViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayForecastViewHolder {

        val binding = ItemWeatherDayAdapterBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return DayForecastViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DayForecastViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class DayForecastViewHolder(private val binding: ItemWeatherDayAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(day: DayForecast) {
            binding.apply {
                textDay.text = day.day.name
                textTemperatureValue.text = "Max: ${day.temp.first} / Min: ${day.temp.second}"
                textUVindexValue.text = day.uvIndex.name
            }
        }
    }

    companion object {
        val COMPARATOR = object : DiffUtil.ItemCallback<DayForecast>() {

            override fun areItemsTheSame(oldItem: DayForecast, newItem: DayForecast): Boolean =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: DayForecast, newItem: DayForecast): Boolean =
                oldItem.day == newItem.day
        }
    }
}
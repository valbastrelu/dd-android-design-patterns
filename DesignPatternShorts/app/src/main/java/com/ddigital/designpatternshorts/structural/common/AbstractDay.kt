package com.ddigital.designpatternshorts.structural.common

import com.ddigital.designpatternshorts.structural.bridge.Forecast

abstract class AbstractDay(
    protected val forecast: Forecast
) {
    override fun equals(other: Any?): Boolean {
        return super.equals(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    abstract fun getTemp(): Pair<Int, Int>
    abstract fun getUvIndex(): String
}

class Monday(forecast: Forecast): AbstractDay(forecast) {

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.MONDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.MONDAY)
    }
}
class Tuesday(forecast: Forecast): AbstractDay(forecast){

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.TUESDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.TUESDAY)
    }
}
class Thursday(forecast: Forecast): AbstractDay(forecast){

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.THURSDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.THURSDAY)
    }
}
class Wednesday(forecast: Forecast): AbstractDay(forecast){

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.WEDNESDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.WEDNESDAY)
    }
}
class Friday(forecast: Forecast): AbstractDay(forecast){

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.FRIDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.FRIDAY)
    }
}
class Saturday(forecast: Forecast): AbstractDay(forecast){

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.SATURDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.SATURDAY)
    }
}
class Sunday(forecast: Forecast): AbstractDay(forecast){

    override fun getTemp(): Pair<Int, Int> {
        return forecast.getTempValues(Day.SUNDAY)
    }

    override fun getUvIndex(): String {
        return forecast.getUvIndex(Day.SUNDAY)
    }
}
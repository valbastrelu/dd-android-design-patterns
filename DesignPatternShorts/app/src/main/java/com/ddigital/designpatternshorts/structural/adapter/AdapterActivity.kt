package com.ddigital.designpatternshorts.structural.adapter

import android.os.Bundle
import androidx.lifecycle.coroutineScope
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.databinding.ActivityAdapterBinding
import com.ddigital.designpatternshorts.utils.DesignPatternType
import kotlinx.coroutines.flow.collect

class AdapterActivity: DiagramMenuActivity() {

    private val repository: WeatherRepository = WeatherRepository()

    //ViewModel

    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.Adapter)
    override val containerId: Int
        get() = -1

    private lateinit var binding: ActivityAdapterBinding
    private val adapter: WeatherRecyclerViewAdapter by lazy {
        WeatherRecyclerViewAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAdapterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerViewWeather.adapter = adapter
        lifecycle.coroutineScope.launchWhenCreated {
            repository.fetchForecast()
            repository.weekFlow.collect { forecastList ->
                adapter.submitList(forecastList)
            }
        }
    }
}


package com.ddigital.designpatternshorts.structural.common

//Obsolete
object TemperatureService : TService {

    override fun getTemp(day: Day): Pair<Int, Int> {
       return when (day) {
            Day.MONDAY -> Pair(32, 12)
            Day.TUESDAY -> Pair(30, 10)
            Day.THURSDAY -> Pair(30, 4)
            Day.WEDNESDAY -> Pair(31, 9)
            Day.FRIDAY -> Pair(12, 2)
            Day.SATURDAY -> Pair(20, 12)
            Day.SUNDAY -> Pair(2, -10)
        }
    }
}
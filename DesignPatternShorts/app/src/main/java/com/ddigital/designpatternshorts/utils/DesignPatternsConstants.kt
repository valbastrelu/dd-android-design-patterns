package com.ddigital.designpatternshorts.utils

object DesignPatternsConstants {
    val creational = DesignPatternType.getCreationals()
    val structural = DesignPatternType.getStructurals()
    val behavioural = DesignPatternType.getBehaviourals()

    val tabs = listOf(creational, structural, behavioural)
    val tabTitles = listOf("creational", "structural", "behavioural")
}

sealed class DesignPatternType(
    val prettyName: String,
) {

    //Creational
    object AbstractFactory : DesignPatternType("Abstract Factory")
    object FactoryMethod : DesignPatternType("Factory method")
    object Builder : DesignPatternType("Builder")
    object Singleton : DesignPatternType("Singleton")
    object Prototype : DesignPatternType("Prototype")

    //Structural
    object Adapter: DesignPatternType("Adapter")
    object Bridge: DesignPatternType("Bridge")
    object Composite: DesignPatternType("Composite")

    companion object {
        fun getCreationals(): List<DesignPatternType> {
            return listOf(AbstractFactory, FactoryMethod, Builder, Singleton, Prototype)
        }

        fun getStructurals(): List<DesignPatternType> {
            return listOf(Adapter, Bridge, Composite)
        }

        fun getBehaviourals(): List<DesignPatternType> {
            return listOf(AbstractFactory)
        }
    }

    object Formatter {
        fun toPath(type: DesignPatternType): String {
            return type.prettyName.lowercase().replace(' ', '_')
        }
    }
}

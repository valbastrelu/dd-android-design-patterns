package com.ddigital.designpatternshorts.creational.factoryMethod

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.fragment.app.Fragment
import com.ddigital.designpatternshorts.R

abstract class RegisterFragment(private val background: Int, private val message: String): Fragment() {

    abstract fun setBackground(@ColorRes colorId: Int)
    abstract fun setMessage(message: String)

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_factory_method, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setBackground(background)
        setMessage(message)
    }
}
package com.ddigital.designpatternshorts.creational.abstractFactory.purple

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.creational.abstractFactory.AbstractColorFactory


class PurpleFragment : Fragment() {

    private lateinit var factory: AbstractColorFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        factory = PurpleFactory()
        return inflater.inflate(R.layout.fragment_purple, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.openDialogFragment).setOnClickListener {
            factory.createDialogFragment().show(parentFragmentManager, "TAG")
        }
    }
}
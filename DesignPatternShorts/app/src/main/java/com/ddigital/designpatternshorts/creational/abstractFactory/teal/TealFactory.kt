package com.ddigital.designpatternshorts.creational.abstractFactory.teal

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.ddigital.designpatternshorts.creational.abstractFactory.AbstractColorFactory

class TealFactory : AbstractColorFactory {
    override fun createFragment(): Fragment {
        return TealFragment()
    }

    override fun createDialogFragment(): DialogFragment {
        return TealDialogFragment()
    }

}
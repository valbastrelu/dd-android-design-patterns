package com.ddigital.designpatternshorts.creational.abstractFactory.teal

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.creational.abstractFactory.AbstractColorFactory


class TealFragment : Fragment() {

    private lateinit var factory: AbstractColorFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        factory = TealFactory()
        return inflater.inflate(R.layout.fragment_teal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<Button>(R.id.openDialogFragment).setOnClickListener {
            factory.createDialogFragment().show(parentFragmentManager, "TAG")
        }
    }
}
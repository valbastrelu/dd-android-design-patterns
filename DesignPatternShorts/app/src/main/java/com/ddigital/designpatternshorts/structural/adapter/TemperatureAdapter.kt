package com.ddigital.designpatternshorts.structural.adapter

import com.ddigital.designpatternshorts.structural.common.Day
import com.ddigital.designpatternshorts.structural.common.NEWTemperatureService
import com.ddigital.designpatternshorts.structural.common.TService

class TemperatureAdapter(
    private val tempService: NEWTemperatureService
) : TService {

    override fun getTemp(day: Day): Pair<Int, Int> {
        val temp = tempService.getTemp(day)
        val tempPair = temp.split(',')
        if (tempPair.size > 1) {
            return Pair(tempPair[0].trim().toInt(), tempPair[1].trim().toInt())
        }
        return Pair(0, 0)
    }
}
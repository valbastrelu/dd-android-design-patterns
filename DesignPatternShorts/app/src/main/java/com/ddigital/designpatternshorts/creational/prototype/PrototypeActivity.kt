package com.ddigital.designpatternshorts.creational.prototype

import android.os.Bundle
import android.widget.TextView
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.utils.DesignPatternType

class PrototypeActivity : DiagramMenuActivity() {

    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.Prototype)
    override val containerId: Int
        get() = R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prototype)

        val adriana = Person("Adriana")
        adriana.owned = Ownership("N/A", "Bugatti")
        val clona = adriana.clone() as Person

        val original = findViewById<TextView>(R.id.textPrototype)
        val clone = findViewById<TextView>(R.id.textPrototypeClone)

        original.applyText("Adriana", adriana)
        clone.applyText("Clona", clona)
    }

    private fun String.simplifyClassName(): String {
        val lastIndexOf = this.lastIndexOf('.')
        return this.substring(lastIndexOf + 1, this.length)
    }

    private fun TextView.applyText(name: String, prototype: Person) {
        val classRef: String = prototype.toString()
        val ownedRef: String = prototype.owned.toString()
        val rentalRef: String = prototype.rented.toString()
        val builder = StringBuilder()
            .append("$name \n\n")
            .append("${classRef.simplifyClassName()}\n\n")
            .append("${ownedRef.simplifyClassName()}\n\n")
            .append("${rentalRef.simplifyClassName()}\n")
        this.text = builder
    }
}
package com.ddigital.designpatternshorts.creational.prototype

class Ownership(
    private val smartPhone: String = "",
    private val laptop: String = ""
) : Prototype {

    constructor(newOwnership: Ownership) : this(
        smartPhone = newOwnership.smartPhone,
        laptop = newOwnership.laptop
    )

    override fun clone(): Prototype {
        return Ownership(this)
    }

}
package com.ddigital.designpatternshorts.creational.builder

import android.os.Bundle
import androidx.fragment.app.Fragment
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.core.DiagramMenuActivity
import com.ddigital.designpatternshorts.utils.DesignPatternType

class BuilderActivity : DiagramMenuActivity() {

    override val umlImageName: String
        get() = DesignPatternType.Formatter.toPath(DesignPatternType.Builder)
    override val containerId: Int
        get() = R.id.container

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abstract_factory)

        if (savedInstanceState == null) {
            replaceFragment(createFragment())
        }
    }

    private fun createFragment(): Fragment = FormBuilder()
        .addNameField("Builder test name") //onAttach
        .addSurnameField() //onAttach ....onViewCreated()
        .addAgeField()
        .addSexField()
        .addCountryField()
        .addAddressField() //onCreateView ...onViewCreated()
        .addPhoneField() //onStart()
        .build()
}

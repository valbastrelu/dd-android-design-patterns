package com.ddigital.designpatternshorts.creational.builder

data class FormParams(
    var name: String? = null,
    var surname: String? = null,
    var age: String? = null,
    var sex: String? = null,
    var country: String? = null,
    var address: String? = null,
    var phone: String? = "002848923"
)

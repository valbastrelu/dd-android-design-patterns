package com.ddigital.designpatternshorts.structural.composite

import android.graphics.Canvas

open class CompositeDots : Graphic {

    val graphicChildren = arrayListOf<Graphic>()

    override fun drawGraphic(canvas: Canvas?) {
        graphicChildren.forEach { dot ->
            dot.drawGraphic(canvas)
        }
    }

    override fun toString(): String {
        return graphicChildren.map { it.toString() }.toString()
    }
}
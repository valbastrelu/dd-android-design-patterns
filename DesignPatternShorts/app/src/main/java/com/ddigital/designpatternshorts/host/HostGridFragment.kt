package com.ddigital.designpatternshorts.host

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.creational.abstractFactory.AbstractFactoryActivity
import com.ddigital.designpatternshorts.creational.builder.BuilderActivity
import com.ddigital.designpatternshorts.creational.factoryMethod.FactoryMethodActivity
import com.ddigital.designpatternshorts.creational.prototype.PrototypeActivity
import com.ddigital.designpatternshorts.structural.adapter.AdapterActivity
import com.ddigital.designpatternshorts.structural.bridge.BridgeActivity
import com.ddigital.designpatternshorts.structural.composite.CompositeActivity
import com.ddigital.designpatternshorts.utils.DesignPatternType

class HostGridFragment(
    private val category: List<DesignPatternType>
): Fragment() {

    private val onItemClicked = View.OnClickListener {
        when (it.tag as DesignPatternType) {
            DesignPatternType.AbstractFactory -> navigateTo(AbstractFactoryActivity::class.java)
            DesignPatternType.Builder -> navigateTo(BuilderActivity::class.java)
            DesignPatternType.FactoryMethod -> navigateTo(FactoryMethodActivity::class.java)
            DesignPatternType.Singleton -> {
                Toast.makeText(requireContext(),
                    "Singleton implemented as FormPreferences",
                    Toast.LENGTH_SHORT).show()
            }
            DesignPatternType.Prototype -> navigateTo(PrototypeActivity::class.java)
            DesignPatternType.Adapter -> navigateTo(AdapterActivity::class.java)
            DesignPatternType.Bridge -> navigateTo(BridgeActivity::class.java)
            DesignPatternType.Composite -> navigateTo(CompositeActivity::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_host_grid_patterns, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val rv = view.findViewById<RecyclerView>(R.id.rvDesignPatterns)
        val designPatternGridAdapter = HostGridAdapter(onItemClicked)
        designPatternGridAdapter.dpItem = category
        rv.adapter = designPatternGridAdapter
        rv.layoutManager = GridLayoutManager(context, 2, GridLayoutManager.VERTICAL, false)
    }

    private fun navigateTo(clazz: Class<*>) {
        startActivity(Intent(requireActivity(), clazz))
    }
}

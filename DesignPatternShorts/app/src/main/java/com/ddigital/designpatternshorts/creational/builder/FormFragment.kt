package com.ddigital.designpatternshorts.creational.builder

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.ddigital.designpatternshorts.R
import com.ddigital.designpatternshorts.creational.singleton.FormPreferences
import com.ddigital.designpatternshorts.utils.find

class FormFragment : Fragment() {

    private val formPrefs = FormPreferences

    var params = FormParams()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setParamsToView(params)

        formPrefs.savePrefs(params)
    }

    override fun onResume() {
        super.onResume()
        val loadPrefs = formPrefs.loadPrefs()
        setParamsToView(loadPrefs)
    }

    private fun setParamsToView(formParams: FormParams) {
        formParams.apply {
            name?.let { addView(it) }
            surname?.let { addView(it) }
            age?.let { addView(it) }
            sex?.let { addView(it) }
            country?.let { addView(it) }
            address?.let { addView(it) }
            phone?.let { addView(it) }
        }
    }

    private fun addView(txt: String) {
        val formList = find<LinearLayout>(R.id.formList)
        val nameField = TextView(context)
        nameField.id = txt.hashCode()
        nameField.layoutParams =
            LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        nameField.text = txt
        formList?.addView(nameField)
    }
}

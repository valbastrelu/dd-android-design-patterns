package com.ddigital.designpatternshorts.creational.prototype

class Rental(
    private val house: String = "",
    private val car: String = ""
)
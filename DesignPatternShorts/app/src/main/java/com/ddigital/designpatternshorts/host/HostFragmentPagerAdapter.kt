package com.ddigital.designpatternshorts.host

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ddigital.designpatternshorts.utils.DesignPatternsConstants

class HostFragmentPagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int = NUM_TABS

    override fun createFragment(position: Int): Fragment {
        return HostGridFragment(DesignPatternsConstants.tabs[position])
    }

    companion object {
        private const val NUM_TABS = 3
    }
}

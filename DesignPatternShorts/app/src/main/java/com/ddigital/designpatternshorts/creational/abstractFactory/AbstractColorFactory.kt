package com.ddigital.designpatternshorts.creational.abstractFactory

import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment

interface AbstractColorFactory {
    fun createFragment(): Fragment
    fun createDialogFragment(): DialogFragment
}
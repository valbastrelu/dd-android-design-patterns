package com.ddigital.designpatternshorts.structural.composite

import android.graphics.Canvas

interface Graphic {
    fun drawGraphic(canvas: Canvas?)
}